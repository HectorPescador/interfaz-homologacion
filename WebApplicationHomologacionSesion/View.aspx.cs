﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using WebApplicationHomologacionSesion;
namespace WebAppHomologacion
{
    public partial class View : System.Web.UI.Page
    {
        //private static Lista Lhomologados = new Lista();       
        //private Lista Grid = new Lista();
        //private static Filas persistLista = new Filas();
        private static GridViewRow SelectedColaHomologacion;
        private ConexionMySql ConexionDropdown = new ConexionMySql();
        private ConexionMySql ConexionGrid = new ConexionMySql();
        private ConexionMySql ConexionInsert = new ConexionMySql();
        protected string Logo;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.Name =="Hector")
            {
                Logo = " <img src = \"Content/Images/AhorraPug.jpg\"; height=\"55\" width=\"55\" class=\"d-inline-block align-top img-fluid\" />";
            }
            else
            {
                Logo = " <img src = \"Content/Images/Ahorra logo.png\"; height=\"55\" width=\"55\" class=\"d-inline-block align-top img-fluid\" />";
            }
            Page.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                LabelUsuario.Text = "Bienvenid@ " +User.Identity.Name;
            }
            if (!IsPostBack)
            {
                LlenaDropdown("aseguradora" , "" , "" , "" , "");                
                LlenaDropdown("marca" , ddlAseguradora.SelectedValue.ToString() , "" , "" , "");
                ColaHomologacion.DataSource = ((Lista)Session["Lhomologados"]).getHhom();
                ColaHomologacion.DataBind();               
            }
            
        }
        protected void ddlAseguradora_SelectedIndexChanged(object sender, EventArgs e)            
        {
            QuerysGridView.DataSource = null;
            QuerysGridView.DataBind();
            Print("");
            Print2("");
            PrintLen();
            LlenaDropdown("marca", ddlAseguradora.SelectedValue.ToString(), "", "", "");

            if (Persist.Checked)
            {

                if (((Lista) Session["Grid"]).ExistMarca(((Filas)Session["persistLista"]).Marca))
                {
                    try
                    {
                         ddlMarca.SelectedIndex = ((Lista)Session["Lhomologados"]).ValueMarca(((Filas)Session["persistLista"]).Marca);
                         ddlMarca.SelectedValue = ((Filas)Session["persistLista"]).Marca;
                    }
                    catch (Exception)
                    {
                        Print(((Filas)Session["persistLista"]).Marca + " no existe en la aseguradora " + ddlAseguradora.SelectedItem.Text +". <br/> Intenta Seleccionar otra marca parecida.");
                    }
                   
                }
                LlenaDropdown("submarca" , ddlAseguradora.SelectedValue.ToString() , ((Filas)Session["persistLista"]).Marca , "" , "");
                if (((Lista) Session["Grid"]).ExistSubmarca(((Filas)Session["persistLista"]).Submarca))
                {
                    try
                    {
                        ddlSubmarca.SelectedIndex = ((Lista)Session["Lhomologados"]).ValueSubmarca(((Filas)Session["persistLista"]).Submarca);
                        ddlSubmarca.SelectedValue = ((Filas)Session["persistLista"]).Submarca;
                    }
                    catch (Exception)
                    {
                        Print(((Filas)Session["persistLista"]).Submarca+" no existe en la aseguradora " + ddlAseguradora.SelectedItem.Text + ". <br/> Intenta Seleccionar otra submarca parecida.");
                    }
                    
                }
                LlenaDropdown("modelo" , ddlAseguradora.SelectedValue.ToString() , ((Filas)Session["persistLista"]).Marca , ((Filas)Session["persistLista"]).Submarca , "");
            }            
           
        }
        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            LlenaDropdown("submarca" , ddlAseguradora.SelectedValue.ToString() , ddlMarca.SelectedValue.ToString() , "" , "");

        }
        protected void ddlSubmarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            //((Filas)Session["persistLista"]).Submarca = ddlSubmarca.SelectedValue.ToString();
            LlenaDropdown("modelo" , ddlAseguradora.SelectedValue.ToString() , ddlMarca.SelectedValue.ToString() , ddlSubmarca.SelectedValue.ToString() , "");
        }
        protected void ddlModelo_SelectedIndexChanged(object sender, EventArgs e)
        {

            LlenaDropdown("" , ddlAseguradora.SelectedValue.ToString() , ddlMarca.SelectedValue.ToString() , ddlSubmarca.SelectedValue.ToString()
                        , ddlModelo.SelectedValue.ToString());
        }
        //-------------------grid master-------------------
        protected void GridMaster_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void GridMaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='aquamarine';";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white';";
               
            }
        }
        protected void GridMaster_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }       
        //------------------------------------------------------
        protected void CheckBoxSelect_CheckedChanged(object sender, EventArgs e)
        {
           
        }
        protected void ButtonInsertar_Click(object sender, EventArgs e)
        {
            GetSelectCheckBox();
            ColaHomologacion.DataSource = ((Lista)Session["Lhomologados"]).getHhom();
            ColaHomologacion.DataBind();
            if (((Lista)Session["Lhomologados"]).BuscarIdMasterBool())
            {
                PrintID("ID Master: " + Convert.ToString(((Lista)Session["Lhomologados"]).BuscarIdMaster()));
                Persist.Checked = true;
            }
            else
            {
                PrintID("Master No Selecionado");
                Persist.Checked = false;
            }
            PrintConteo("Elementos: " + Convert.ToString(((Lista)Session["Lhomologados"]).len()));
            Print("");
        }
        protected void ColaHomologacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedColaHomologacion = ColaHomologacion.SelectedRow;
        }
        protected void BtnHomologar_Click(object sender, EventArgs e)
        {
            string QueryDescripcionCorta = "INSERT INTO version_front VALUES("+((Lista)Session["Lhomologados"]).BuscarIdMaster()+", '"+HomologacionR.Text+"')";
            //Print(QueryDescripcionCorta);
            ConexionInsert.DSET(QueryDescripcionCorta);
            ConexionInsert.Cerrar();

            string QueryAseguradoras,toprint,queryHomologadores;
            List<string> Querys = new List<string>();
            int z =((Lista)Session["Lhomologados"]).BuscarIdMaster();
            if (z==-1)
            {
                Print("Error, hay mas de una fila del master seleccionada");
            }
            else
            {
                for (int i = 0; i < ((Lista)Session["Lhomologados"]).len(); i++)
                {
                    Filas aux = ((Lista)Session["Lhomologados"]).GetFila(i);
                    QueryAseguradoras = "insert into " + aux.Aseguradora.ToLower()
                        + " (id" + aux.Aseguradora.Substring(0, 1).ToUpper() + aux.Aseguradora.Substring(1).ToLower()
                        + ",IdDhomologada) values(" + aux.Id + "," + z.ToString() + ")";
                    toprint = aux.Aseguradora + ": id"
                        + aux.Aseguradora.Substring(0, 1).ToUpper() + aux.Aseguradora.Substring(1).ToLower() + " :" + aux.Id +
                        ",IdDhomologada: " + z.ToString();
                    Querys.Add(toprint);
                    ConexionInsert.DSET(QueryAseguradoras);
                    ConexionInsert.Cerrar();
                }
                queryHomologadores = "insert into RegistroHomologacion (Homologador,idHomologada,Fecha)" +
                    " values ('" + User.Identity.Name + "'," + z.ToString() + ",'" + DateTime.Now.ToString() + "')";
                ConexionInsert.DSET(queryHomologadores);
                ConexionInsert.Cerrar();

                QuerysGridView.DataSource = Querys;
                QuerysGridView.DataBind();
                Print("La " + MuestraIdMaster.Text + " ha sido homologada correctamente.");
                BorrarPila();
                HomologacionR.Text = "";
            }
        }
        protected void BorrarPila()
        {
            ((Lista)Session["Lhomologados"]).BorrarTodo();
            ColaHomologacion.DataSource = ((Lista)Session["Lhomologados"]).getHhom();
            ColaHomologacion.DataBind();
            if (((Lista)Session["Lhomologados"]).BuscarIdMasterBool())
            {
                PrintID("ID Master: " + Convert.ToString(((Lista)Session["Lhomologados"]).BuscarIdMaster()));
                Persist.Checked = true;
            }
            else
            {
                PrintID("Master No Selecionado");
                Persist.Checked = false;
            }
            PrintConteo("Elementos: " + Convert.ToString(((Lista)Session["Lhomologados"]).len()));
            ColaHomologacion.EditIndex = -1;
        }
        protected void BtnBorrarPila_Click(object sender, EventArgs e)
        {
            ((Lista)Session["Lhomologados"]).BorrarTodo();
            ColaHomologacion.DataSource = ((Lista)Session["Lhomologados"]).getHhom();
            ColaHomologacion.DataBind();
            if (((Lista)Session["Lhomologados"]).BuscarIdMasterBool())
            {
                PrintID("ID Master: " + Convert.ToString(((Lista)Session["Lhomologados"]).BuscarIdMaster()));
                Persist.Checked = true;
            }
            else
            {
                PrintID("Master No Selecionado");
                Persist.Checked = false;
            }
            PrintConteo("Elementos: " + Convert.ToString(((Lista)Session["Lhomologados"]).len()));
            ColaHomologacion.EditIndex = -1;
        }
        protected void SacarDeColaHomologacion_Click(object sender, EventArgs e)
        {
            if (ColaHomologacion.SelectedIndex != -1)
            {
                ((Lista)Session["Lhomologados"]).BorrarFila(Convert.ToInt32(SelectedColaHomologacion.Cells[1].Text));
                ColaHomologacion.DataSource = ((Lista)Session["Lhomologados"]).getHhom();
                ColaHomologacion.DataBind();
            }
            else
            {
                Print("Seleccione una fila para borrar");
            }
           
            if (((Lista)Session["Lhomologados"]).BuscarIdMasterBool())
            {
                PrintID("ID Master: " + Convert.ToString(((Lista)Session["Lhomologados"]).BuscarIdMaster()));
                Persist.Checked = true;
            }
            else
            {
                PrintID("Master No Selecionado");
                Persist.Checked = false;
            }
            PrintConteo("Elementos: " + Convert.ToString(((Lista)Session["Lhomologados"]).len()));
            ColaHomologacion.SelectedIndex = -1;
        }
        protected void HomologacionR_TextChanged(object sender, EventArgs e)
        {
            PrintLen();
        }
        protected void LogOutButton_Click1(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect(Request.UrlReferrer.ToString());
        }
        protected void GetSelectCheckBox()
        {
            foreach (GridViewRow fila in GridMaster.Rows)
            {
                CheckBox check = (fila.Cells[0].FindControl("CheckBoxSelect") as CheckBox);
                if (check.Checked && !((Lista)Session["Lhomologados"]).Buscar(Convert.ToInt32(fila.Cells[1].Text)))
                {
                    Filas fil = new Filas
                    {
                        Id = Convert.ToInt32(fila.Cells[1].Text),
                        Marca = fila.Cells[2].Text,
                        Submarca = fila.Cells[3].Text,
                        Modelo = fila.Cells[4].Text,
                        Descripcion = fila.Cells[5].Text,
                        Aseguradora = ddlAseguradora.SelectedItem.Text
                    };
                    ((Lista)Session["Lhomologados"]).setHhom(fil);
                }
            }     
        }
        public void Print(string entrada)
        {
            Salida.Text = entrada;
        }
        public void Print2(string entrada)
        {
            Salida2.Text = entrada;
        }
        public void PrintID(string en)
        {
            MuestraIdMaster.Text = en;
        }
        public void PrintConteo(string ent)
        {
            ConteoHomologacion.Text = ent;
        }
        public void PrintLen()
        {
            string a = HomologacionR.Text;
            LenHomologacionR.Text = Convert.ToString(a.Length)+ (a.Length == 1?" caracter": " caracteres");
        }
        public void LlenaDropdown(string DropdownOpcion, string Aseguradora, string Marca, string Submarca, string Modelo)
        {

            //ConexionDropdown.Abrir();
            //ConexionGrid.Abrir();
            string QueryAseguradora, QueryMarca, QuerySubmarca, QueryModelo, QueryDrop, QueryGrid;
            if (Aseguradora == "")
            {
                Aseguradora = "QUALITAS";
            }
            QueryAseguradora = " d_" + Aseguradora.ToLower();
            if (Marca != "")
            {
                QueryMarca = " where Marca = '" + Marca + "'";
            }
            else
            {
                QueryMarca = "";
            }
            if (Submarca != "")
            {
                QuerySubmarca = " and Submarca like '%" + Submarca + "%'";
            }
            else
            {
                QuerySubmarca = "";
            }
            if (Modelo != "")
            {
                QueryModelo = " and Modelo = " + Modelo;
            }
            else
            {
                QueryModelo = "";
            }            
            if (DropdownOpcion.ToLower() == "aseguradora")//inicializa llena la lista de aseguradoras
            {
                
                QueryDrop = "select aseguradora from aseguradora order by aseguradora";
                DataTable a = ConexionDropdown.DSET(QueryDrop);
                ddlAseguradora.DataSource = a;
                a.Dispose();
                ddlAseguradora.DataTextField = "aseguradora";
                ddlAseguradora.SelectedIndex = 18;
                ddlAseguradora.DataBind();
                ddlAseguradora.SelectedValue = "QUALITAS";     
                
            }
            else if (DropdownOpcion.ToLower() == "marca")//llena la marca con la aseguradora elegida
            {                                               
                if (ddlAseguradora.SelectedItem.Text == "QUALITAS")
                {
                    QueryDrop = "select distinct marca as Marca from homologacionDatos order by Marca";
                }
                else
                {
                    QueryDrop = "select distinct Marca from" + QueryAseguradora + " order by Marca";

                }
                //vacia la lista de marca
                ddlMarca.ClearSelection();
                ddlMarca.Items.Clear();

                ddlSubmarca.ClearSelection();
                ddlSubmarca.Items.Clear();

                ddlModelo.ClearSelection();
                ddlModelo.Items.Clear();
                //llena marca
                DataTable b = ConexionDropdown.DSET(QueryDrop);
                ddlMarca.DataSource = b;
                b.Dispose();
                ddlMarca.DataTextField = "Marca";
                ddlMarca.Items.Insert(0, new ListItem("<" + ddlAseguradora.SelectedItem + ">", "0"));
                ddlMarca.DataBind();
            }
            else if (DropdownOpcion.ToLower() == "submarca")//llena la submarca con la marca elegida
            {
                if (ddlAseguradora.SelectedItem.Text == "QUALITAS")
                {
                    QueryDrop = "select DISTINCT tipo AS Submarca  from homologacionDatos where marca = '" + Marca + "' order by Submarca";
                }
                else
                {
                    QueryDrop = "select distinct Submarca from" + QueryAseguradora + QueryMarca + " order by Submarca";
                }
                ddlSubmarca.ClearSelection();
                ddlSubmarca.Items.Clear();

                ddlModelo.ClearSelection();
                ddlModelo.Items.Clear();
                //llena submarca
                DataTable c = ConexionDropdown.DSET(QueryDrop);
                ddlSubmarca.DataSource = c;
                c.Dispose();
                ddlSubmarca.DataTextField = "Submarca";
                ddlSubmarca.Items.Insert(0, new ListItem("<" + ddlMarca.SelectedItem + ">", "0"));
                ddlSubmarca.DataBind();
                if (ddlAseguradora.SelectedItem.Text == "QUALITAS")
                {
                    ((Filas)Session["persistLista"]).Marca = Marca;
                    ((Filas)Session["persistLista"]).Submarca = null;
                }
            }
            else if (DropdownOpcion.ToLower() == "modelo") //llena modelo
            {
                if (ddlAseguradora.SelectedItem.Text == "QUALITAS")
                {
                    QueryDrop = "select DISTINCT modelo AS Modelo from homologacionDatos where marca = '" + Marca + "' and tipo like '%"+Submarca+"%' order by Modelo";
                }
                else
                {
                    QueryDrop = "select distinct Modelo from" + QueryAseguradora + QueryMarca + QuerySubmarca + " order by Modelo";
                }
                ddlModelo.ClearSelection();
                ddlModelo.Items.Clear();
                //llena modelo
                DataTable d = ConexionDropdown.DSET(QueryDrop);
                ddlModelo.DataSource = d;
                d.Dispose();
                ddlModelo.DataTextField = "Modelo";
                ddlModelo.Items.Insert(0, new ListItem("<" + ddlSubmarca.SelectedItem + ">", "0"));
                ddlModelo.DataBind();
                if (ddlAseguradora.SelectedItem.Text=="QUALITAS")
                {
                    ((Filas)Session["persistLista"]).Submarca = Submarca;
                }
            }           
            if (DropdownOpcion.ToLower() == "marca")
            {
                if (ddlAseguradora.SelectedItem.Text == "QUALITAS")
                {
                    QueryGrid = "select id_DHomologada as id,marca as Marca,tipo as Submarca,modelo as Modelo,version as Descripcion from homologacionDatos where marca = '"
                         + Marca + "' and tipo like '%" + Submarca + "%' " + QueryModelo + " LIMIT 0,15";
                }
                else
                {
                    QueryGrid = "select id,Marca,Submarca,Modelo,Descripcion from " + QueryAseguradora + QueryMarca + QuerySubmarca + QueryModelo + " LIMIT 0,15";
                }
            }
            else
            {
                if (ddlAseguradora.SelectedItem.Text == "QUALITAS")
                {
                    QueryGrid = "select id_DHomologada as id,marca as Marca,tipo as Submarca,modelo as Modelo,version as Descripcion from homologacionDatos where marca = '"
                         + Marca + "' and tipo like '%" + Submarca + "%' " + QueryModelo;
                }
                else
                {
                    QueryGrid = "select id,Marca,Submarca,Modelo,Descripcion from " + QueryAseguradora + QueryMarca + QuerySubmarca + QueryModelo;
                }
            }
            DataTable e = ConexionGrid.DSET(QueryGrid);
            GridMaster.DataSource = e;
            e.Dispose();
            GridMaster.DataBind();
            foreach (GridViewRow fila in GridMaster.Rows)
            {
                Filas fil = new Filas
                {
                    Id = Convert.ToInt32(fila.Cells[1].Text),
                    Marca = fila.Cells[2].Text,
                    Submarca = fila.Cells[3].Text,
                    Modelo = fila.Cells[4].Text,
                    Descripcion = fila.Cells[5].Text,
                    Aseguradora = ddlAseguradora.SelectedItem.Text
                };
                ((Lista) Session["Grid"]).setHhom(fil);
            }
            ConexionDropdown.Cerrar();
            ConexionGrid.Cerrar();
        }
    }
}