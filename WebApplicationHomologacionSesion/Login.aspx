﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebAppHomologacion.Login" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
</head>
<body>
    <form id="form1" runat="server">        
        <nav class="navbar navbar-dark bg-dark">
              <a class="navbar-brand" href="#">Ahorra Seguros</a>
              <div class="row">
                   <div class="col navbar-nav">
                      <a class="nav-item nav-link active" href="#">Homologación <span class="sr-only">(current)</span></a>
                    </div>                               
              </div> 
            </nav>
        <br />
        <div class="container justify-content-md-center" style="width:100%;">
             <asp:Login runat="server" 
                            ID="Login1" 
                            LoginButtonText="Ingresar" 
                            PasswordLabelText="Contraseña" 
                            RememberMeText="Recordarme la proxima vez." 
                            TitleText="" 
                            UserNameLabelText="Usuario" 
                            OnAuthenticate="Login1_Authenticate" 
                            CssClass="card text-white bg-dark mb-3"
                            style="max-width: 18rem;">                
                        </asp:Login>
        </div>    
       
        
    </form>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
</body>
</html>
