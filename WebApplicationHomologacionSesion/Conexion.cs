﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace WebAppHomologacion
{
    public class Conexion
    {
        

        private string credencial = @"Data Source=SERVER-WS\TRIGARANTE_WS;Initial Catalog=Homologacion;Persist Security Info=True;User ID=servici1;Password=Master@011";
        SqlConnection conexion;
        ///Amazon
        public bool Abrir()
        {
            try
            {
                conexion = new SqlConnection(credencial);
                conexion.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void Cerrar()
        {
            conexion.Close();
        }
        public DataTable DSET(string sentencia)
        {
            DataTable dt = new DataTable();
            if (Abrir())
            {
                try
                {
                    SqlDataAdapter SDA = new SqlDataAdapter(sentencia, conexion);
                    SDA.Fill(dt);
                }
                catch (Exception)
                {
                    
                }
                
            }
            return dt;
        }
        public int IEA(string sentencia)
        {
            int res = -1;
            if (Abrir())
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(sentencia, conexion);
                    cmd.ExecuteNonQuery();
                    res = 0;
                }
                catch (Exception e)
                {
                    int error = Convert.ToInt32(e);
                  
                }
            }
            return res;
        }
    }
    public class ConexionMySql
    {
        private MySqlConnection ConexionAmazon = new MySqlConnection();
        private string CredencialAmazon = @"Server=mark44.cputezxs2i4c.us-east-2.rds.amazonaws.com;Database=homologacionAutos; Uid=servici1;Pwd=UUpzNvi9huoAF4SX;";
         
        public bool Abrir()
        {           
            try
            {
                ConexionAmazon.ConnectionString = CredencialAmazon;
                ConexionAmazon.Open();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public void Cerrar()
        {
            ConexionAmazon.Close();
            
        }
        public DataTable DSET(string sentencia) 
        {
            DataTable dt = new DataTable();
            if (Abrir())
            {
                try
                {
                    MySqlDataAdapter SDA = new MySqlDataAdapter(sentencia,ConexionAmazon);
                    SDA.Fill(dt);
                }
                catch (Exception)
                {
                    
                }                
            }
            return dt;
        }
        public int IEA(string sentencia)
        {
            int res = -1;
            if (Abrir())
            {
                try
                {
                    MySqlCommand cmd = new MySqlCommand(sentencia,ConexionAmazon);
                    cmd.ExecuteNonQuery();
                    res = 0;
                }
                catch (Exception)
                {

                }
            }
            return res;
        }

    }
    public class Lista
    {
        private List<Filas> Homologados = new List<Filas>();
        public List<Filas> getHhom()
        {
            return Homologados;
        }
        public void setHhom(Filas fil)
        {
            this.Homologados.Add(fil);
        }
        public void BorrarFila(int idR)
        {
            Homologados.RemoveAll(x => x.Id == idR);
        }
        public bool Buscar(int idR)
        {
            if (Homologados.Find(y => y.Id == idR) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void BorrarTodo()
        {
            Homologados.Clear();
        }
        public int BuscarIdMaster()
        {
            try
            {
                return Homologados.Find(w => w.Aseguradora == "QUALITAS").Id;

            }
            catch (Exception)
            {
                return -1;
                throw;
            }
        }
       
        public bool BuscarIdMasterBool()
        {
            if (Homologados.Find(w => w.Aseguradora == "QUALITAS") != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
       
        public string GetAseguradora(int indx)
        {
            return Homologados[indx].Aseguradora;
        }


        public Filas GetFila(int indx)
        {
            return Homologados[indx];
        }
        public int len()
        {
            return Homologados.Count();
        }
        public int ValueMarca(string marca)
        {
            if (marca == null)
            {
                return -1;
            }
            else
            {
                return Homologados.FindIndex(w => w.Marca.Contains(marca));
            } 
        }
        public int ValueSubmarca(string submarca)
        {
            if (submarca == null)
            {
                return -1;
            }
            else
            {
            return Homologados.FindIndex(w => w.Submarca.Contains(submarca));
            }
        }
        public int ValueModelo(string modelo)
        {
            if (modelo==null)
            {
                return -1;
            }
            else
            {
                return Homologados.Find(w => w.Modelo.Contains(modelo)).Id;
            }
        }
        public bool ExistMarca(string marca)
        {
            int a = Homologados.FindIndex(q => q.Marca.Contains(marca));
            if (a!=-1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool ExistSubmarca(string submarca)
        {
            if (submarca != null)
            {
                try
                {
                     int a = Homologados.FindIndex(q => q.Submarca.Contains(submarca));
                    if (a != -1)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
            else
            {
                return false;
            }

           
        }
    }
    public class Filas
    {
        public int Id { get; set; }
        public string Marca { get; set; }
        public string Submarca { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public string Aseguradora { get; set; }
    }
   
}



