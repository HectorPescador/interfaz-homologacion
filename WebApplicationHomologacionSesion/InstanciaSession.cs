﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using WebAppHomologacion;

namespace WebApplicationHomologacionSesion
{
    class SesionData
    {
        List<InstanciaSession> Datos = new List<InstanciaSession>();

        public bool ExistSession(string Nombre)
        {
            if (Datos.Find(w => w.NombreSession == Nombre) != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool CreateSession(InstanciaSession a)
        {
            try
            {
                Datos.Add(a);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public InstanciaSession GetSessionData(string Nombre)
        {
            InstanciaSession w = Datos.Find(x => x.NombreSession == Nombre);
            return w;
        }
    }
    class InstanciaSession
    {
        public string NombreSession { get; set; }
        public static Lista Lhomologados { get; set; }
        public Lista Grid { get; set; }
        public static GridViewRow SelectedColaHomologacion { get; set; }
        public static Filas persistLista { get; set; }
    }
}