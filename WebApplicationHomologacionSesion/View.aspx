<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="View.aspx.cs" Inherits="WebAppHomologacion.View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   
    <link rel="icon" href="Content/Images/Ahorra logo.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
    <title>Homologacion</title>    
    
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-dark bg-dark">
      <a class="navbar-brand text-white" href="#">
          <%=Logo %>
          Ahorra Seguros
      </a>
      <div class="row">
           <div class="col navbar-nav">
              <a class="nav-item nav-link active" href="#">Homologación <span class="sr-only">(current)</span></a>
            </div>
          
             <div class="col justify-content-md-end dropdown">
                 <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <asp:Label runat="server" ID="LabelUsuario"></asp:Label>                  
                 </button>
                 <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                     <asp:Button runat="server" CssClass="dropdown-item" Text="Cerrar Sesion" ID="LogOutButton" OnClick="LogOutButton_Click1"/>
                 </div>
             </div>
      </div>
       
          
    
    </nav>
    
        <div class="">
            <div class="row">
                <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
                <asp:UpdatePanel runat="server" ID="UpdatePanelGridMaster" UpdateMode="Always" class="col-6 text-center" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div>
                           <div class="">                       
                               <div class="row">
                                   <div class="col row">
                                       <div class="badge badge-light col">Aseguradora</div>
                                       <div class="w-100"></div>
                                       <div class="col">
                                           <asp:DropDownList CssClass="form-control-sm" ID="ddlAseguradora" runat="server" OnSelectedIndexChanged="ddlAseguradora_SelectedIndexChanged"  AppendDataBoundItems="True" AutoPostBack="True" style="height: 22px" Font-Size="X-Small">
                                                <asp:ListItem Value="0">Ninguno</asp:ListItem>
                                            </asp:DropDownList> 
                                       </div>
                               
                                   </div>
                                   <div class="col row">
                                       <div class="badge badge-light col">Marca</div>
                                       <div class="w-100"></div>
                                       <div class="col">
                                            <asp:DropDownList CssClass="form-control-sm" ID="ddlMarca" runat="server" OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True" Font-Size="X-Small" >
                                                <asp:ListItem Value="0">Ninguno</asp:ListItem>
                                            </asp:DropDownList>
                                       </div>
                               
                                   </div>
                                   <div class="col row">
                                       <div class="badge badge-light col">Submarca</div>
                                       <div class="w-100"></div>
                                       <div class="col">
                                            <asp:DropDownList CssClass="form-control-sm" ID="ddlSubmarca" runat="server" OnSelectedIndexChanged="ddlSubmarca_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True" Font-Size="X-Small">
                                                <asp:ListItem Value="0">Ninguno</asp:ListItem>
                                            </asp:DropDownList>
                                       </div> 
                               
                                   </div>
                                   <div class="col row">
                                       <div class="badge badge-light col">Modelo</div>
                                       <div class="w-100"></div>
                                       <div class="col ">
                                           <asp:DropDownList CssClass="form-control-sm" ID="ddlModelo" runat="server"  AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ddlModelo_SelectedIndexChanged" Font-Size="X-Small">
                                                <asp:ListItem Value="0">Ninguno</asp:ListItem>
                                            </asp:DropDownList>
                                       </div> 
                               
                                   </div>
                               </div>
                           </div>
                            <asp:GridView ID="GridMaster" runat="server"  CellPadding="4" ForeColor="Black" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" OnSelectedIndexChanged="GridMaster_SelectedIndexChanged" HorizontalAlign="Center" PageSize="15" AllowSorting="True" OnRowDataBound="GridMaster_RowDataBound" OnRowCreated="GridMaster_RowCreated">
                                <AlternatingRowStyle BackColor="White" />
                
                                <Columns>
                                    <asp:TemplateField HeaderText="Seleccionar">
                                           <ItemTemplate>
                                               <asp:CheckBox runat="server" OnCheckedChanged="CheckBoxSelect_CheckedChanged" ID="CheckBoxSelect" />
                                           </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle Font-Size="X-Small" />
                                <FooterStyle BackColor="#CCCC99" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" Font-Size="Small" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <RowStyle Font-Size="X-Small" />
                                <SelectedRowStyle BackColor="#003366" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                <SortedDescendingHeaderStyle BackColor="#575357" />
               
                            </asp:GridView>
                            <div class="container">
                                <asp:Button CssClass="btn btn-dark" ID="BtnInsertar" runat="server" OnClick="ButtonInsertar_Click" Text="Insertar" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                





                <asp:UpdatePanel runat="server" ID="UpdatePanelColaHomologacion" class=" col-6" UpdateMode="Always" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div class="card text-center text-white bg-dark mb-3">
                        <div class="card-header">Pila de registros Homologados</div>
                         <div class="row">
                             <div class="col">
                                 <asp:Label ID="MuestraIdMaster" runat="server" Text="Master No Seleccionado" CssClass="badge badge-secondary"></asp:Label>
                             </div>
                             <div class="col">
                                 <asp:Label ID="ConteoHomologacion" runat="server" Text="Elementos: 0" CssClass="badge badge-secondary"></asp:Label>

                             </div>
                             <div class="col">
                                 <asp:CheckBox ID="Persist" runat="server"/>Persist

                             </div>
                         </div>
                        <div class="card-body">
                            <asp:GridView ID="ColaHomologacion" 
                                runat="server" 
                                autogenerateselectbutton="True"
                                BackColor="White" 
                                BorderColor="#DEDFDE" 
                                BorderStyle="None" 
                                BorderWidth="1px" 
                                CellPadding="4" 
                                ForeColor="Black" 
                                GridLines="Vertical" 
                                HorizontalAlign="Center" 
                                OnSelectedIndexChanged="ColaHomologacion_SelectedIndexChanged" 
                                DataKeyNames="Id">                                
                                <AlternatingRowStyle BackColor="White" />                             
                                <FooterStyle BackColor="#CCCC99" />
                                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" Font-Size="Small" />
                                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                                <RowStyle Font-Size="X-Small" />
                                <SelectedRowStyle BackColor="Gray" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                                <SortedAscendingHeaderStyle BackColor="#848384" />
                                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                                <SortedDescendingHeaderStyle BackColor="#575357" />
                            </asp:GridView>
                            <div class="container">
                                
                            </div>
                            <div class="container">
                                <asp:Button CssClass="btn btn-danger btn-sm" ID="BtnBorrarPila" Text="Borrar Todo" OnClick="BtnBorrarPila_Click" runat="server"/>
                                <asp:Button CssClass="btn btn-light btn-sm" ID="BtnHomologar" runat="server" Text="Homologar" OnClick="BtnHomologar_Click"/>
                                <asp:Button CssClass="btn btn-danger btn-sm" ID="SacarDeColaHomologacion" runat="server" Text="Quitar" OnClick="SacarDeColaHomologacion_Click"/>
                                <asp:TextBox runat="server" ID="HomologacionR" ToolTip="Homologacion Recortada" MaxLength="30" AutoPostBack="True"  OnTextChanged="HomologacionR_TextChanged">
                                </asp:TextBox>
                                <asp:Label runat="server" ID="LenHomologacionR" Text="0 caracteres"></asp:Label>
                               
                            </div>
                        </div>
                    </div> 
                    </div>
                        <asp:Label id="Salida" runat="server"></asp:Label>
                        <asp:Label id="Salida2" runat="server"></asp:Label>                        
                    </div>
                    <asp:GridView runat="server" ID="QuerysGridView"
                            BackColor="White" 
                            BorderColor="#DEDFDE" 
                            BorderStyle="None" 
                            BorderWidth="1px" 
                            CellPadding="4" 
                            ForeColor="Black" 
                            GridLines="Vertical" 
                            HorizontalAlign="Center" >
                            
                            <AlternatingRowStyle BackColor="White" />                             
                            <FooterStyle BackColor="#CCCC99" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" Font-Size="Small" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <RowStyle Font-Size="X-Small" />
                            <SelectedRowStyle BackColor="Gray" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#FBFBF2" />
                            <SortedAscendingHeaderStyle BackColor="#848384" />
                            <SortedDescendingCellStyle BackColor="#EAEAD3" />
                            <SortedDescendingHeaderStyle BackColor="#575357" />
                    </asp:GridView>
                    </ContentTemplate>
                    
                </asp:UpdatePanel>
           
       
          </div>
      </div>                     
    </form>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    
   
</body>
</html>
