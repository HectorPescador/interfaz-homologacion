﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppHomologacion
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {            
            if (Autentificacion.Autenticar(Login1.UserName, Login1.Password))
            {
                
                FormsAuthentication.RedirectFromLoginPage(Login1.UserName, Login1.RememberMeSet);                  
                 
            }
        }
    }
}